﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter your text:\n";
    std::string text;
    std::getline(std::cin, text);

    std::cout << text.length() << "\n";
    std::cout << text.front() << "\n";
    std::cout << text.back() << "\n";

    std::cin;
    return 0;
}